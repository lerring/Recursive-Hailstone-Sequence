package HS;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Hailstone {

	static Scanner console = new Scanner(System.in);

	
	public static void main(String[] args) {

		int answer = userInterface();
		
		hailstoneSequence(answer, false);
	}

	private static int hailstoneSequence(int n, boolean repeated) {

		System.out.println(n);
		
		if(n == 1 && !repeated){
			
			repeated = true;
			return hailstoneSequence((n * 3) + 1, repeated);
			
		}
		if(n != 1){
		
		if (n % 2 == 0)
			return hailstoneSequence(n / 2, repeated);
		if (n % 2 == 1)
			return hailstoneSequence((n * 3) + 1, repeated);
		
		}
		
		
		return n;
	}


	private static int userInterface() {

		System.out.println("Enter a starting integer:");
	
		while (true){
			try {
				int input = console.nextInt();
				console.close();
				return input;
			} catch (InputMismatchException e) {
				System.out.println("Please input an Integer value!");
				console.next();
			}
		}
	}

	

}
